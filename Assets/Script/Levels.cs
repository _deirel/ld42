﻿using System.Collections.Generic;

public class Levels
{
    private static List<string> levels = new List<string>(new string[]
    {
        "Level 1",
        "Level 2"
    });
    
    private static Dictionary<string, bool> passedMap = new Dictionary<string, bool>();

    public static string GetNextLevelName(string name)
    {
        var index = levels.IndexOf(name);
        return (index > -1 && index < levels.Count - 1) ? levels[index + 1] : null;
    }

    public static void SetPassed(string name)
    {
        passedMap[name] = true;
    }

    public static bool IsLevelPassed(string name)
    {
        return passedMap.ContainsKey(name);
    }

    public static string StartLevelName
    {
        get { return "Level Start"; }
    }
}