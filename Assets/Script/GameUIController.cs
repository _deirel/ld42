﻿using Objects;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public GameObject selectionPanel;
    public Text selectedObjectName;
    public Text selectedObjectUsage;

    public GameObject exitGamePanel;
    public Button exitYes;
    public Button exitNope;

    private void Start()
    {
        exitYes.onClick.AddListener(OnExitYes);
        exitNope.onClick.AddListener(OnExitNope);
        
        NotificationCenter.AddHandler<GameObject>(GUINotifications.SelectedObjectChange, OnSelectedObjectChange);
        OnSelectedObjectChange(null);
    }

    private void OnSelectedObjectChange(GameObject target)
    {
        if (target == null)
        {
            selectionPanel.SetActive(false);
        }
        else
        {
            IInteraction interaction = target.GetComponent<IInteraction>();
            if (interaction == null)
                return;

            selectionPanel.SetActive(true);

            selectedObjectName.text = interaction.InteractionObjectName;
            selectedObjectUsage.text = interaction.InteractionObjectUsage;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            exitGamePanel.SetActive(!exitGamePanel.activeSelf);
        }
    }

    private void OnExitYes()
    {
        Application.Quit();
    }

    private void OnExitNope()
    {
        exitGamePanel.SetActive(false);
    }

    private void OnDestroy()
    {
        exitYes.onClick.RemoveAllListeners();
        exitNope.onClick.RemoveAllListeners();
        NotificationCenter.RemoveHandler<GameObject>(GUINotifications.SelectedObjectChange, OnSelectedObjectChange);
    }
}