﻿using System;
using System.Collections.Generic;

public class NotificationCenter
{
    private static Dictionary<String, List<object>> handlers = new Dictionary<string, List<object>>();

    public static void AddHandler<T>(String type, Action<T> handler)
    {
        var hs = handlers.ContainsKey(type) ? handlers[type] : handlers[type] = new List<object>();
        hs.Add(handler);
    }

    public static void AddHandler(String type, Action handler)
    {
        var hs = handlers.ContainsKey(type) ? handlers[type] : handlers[type] = new List<object>();
        hs.Add(handler);
    }

    public static void PostNotification<T>(String type, T data)
    {
        if (!handlers.ContainsKey(type))
            return;

        var hs = handlers[type];
        foreach (var handler in hs)
        {
            if (handler is Action<T>)
            {
                ((Action<T>) handler)(data);
            }
            else
            {
                ((Action) handler)();
            }
        }
    }

    public static void PostNotification(String type)
    {
        if (!handlers.ContainsKey(type))
            return;

        var hs = handlers[type];
        foreach (var handler in hs)
        {
            if (handler is Action<object>)
            {
                ((Action<object>) handler)(null);
            }
            else
            {
                ((Action) handler)();
            }
        }
    }

    public static void RemoveHandler(String type, Action handler)
    {
        if (!handlers.ContainsKey(type))
            return;

        var hs = handlers[type];
        hs.Remove(handler);
    }

    public static void RemoveHandler<T>(String type, Action<T> handler)
    {
        if (!handlers.ContainsKey(type))
            return;

        var hs = handlers[type];
        hs.Remove(handler);
    }
}