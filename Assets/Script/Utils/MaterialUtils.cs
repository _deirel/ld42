﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Utils
{
    public class MaterialUtils
    {
        public static void SetMaterialModeTransparent(Material m)
        {
//            m.SetFloat("_Mode", 2f);
//            m.SetInt("_SrcBlend", (int) BlendMode.SrcAlpha);
//            m.SetInt("_DstBlend", (int) BlendMode.OneMinusSrcAlpha);
//            m.SetInt("_ZWrite", 0);
//            m.DisableKeyword("_ALPHATEST_ON");
//            m.EnableKeyword("_ALPHABLEND_ON");
//            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
//            m.renderQueue = 3000;
        }

        public static void SetMaterialTransparency(Material m, float value)
        {
            var color = m.color;
            m.color = new Color(color.r, color.g, color.b, value);
        }
    }
}