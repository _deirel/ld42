﻿using UnityEngine;

namespace Utils
{
    public class LevelUtils
    {
        private static readonly float GameObjectOffsetX = .5f;
        private static readonly float GameObjectOffsetZ = .5f;

        public static int GetObjectX(GameObject obj)
        {
            return Mathf.FloorToInt(obj.transform.position.x + GameObjectOffsetX);
        }

        public static int GetObjectZ(GameObject obj)
        {
            return Mathf.FloorToInt(obj.transform.position.z + GameObjectOffsetZ);
        }

        public static void SetObjectXZ(GameObject obj, int x, int z)
        {
            var position = obj.transform.position;
            position.x = x;
            position.z = z;
            obj.transform.position = position;
        }
    }
}