﻿using Objects;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    public float distance = 1f;

    private GameObject currentTarget;
    private bool targetWasNull = true;

    private void Update()
    {
        UpdateTarget();
        ProcessInteraction();
    }

    private void UpdateTarget()
    {
        GameObject target = null;

        var cameraTransform = Camera.main.transform;
        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, distance))
        {
            target = hit.collider.gameObject;
        }

        if (target == currentTarget)
        {
            if (!targetWasNull && target == null)
            {
                targetWasNull = true;
                NotificationCenter.PostNotification(GUINotifications.SelectedObjectChange, target);                
            }
            
            return;
        }

        targetWasNull = target == null;

        if (currentTarget != null)
            OnTargetExit(currentTarget);

        currentTarget = target;

        if (currentTarget != null)
            OnTargetEnter(currentTarget);
        
        NotificationCenter.PostNotification(GUINotifications.SelectedObjectChange, currentTarget);
    }

    private void ProcessInteraction()
    {
        if (currentTarget == null || !Input.GetButtonDown("Interaction"))
            return;

        var interactions = currentTarget.GetComponents<IInteraction>();
        foreach (var interaction in interactions)
        {
            interaction.Interact(Game.Player);
        }
    }

    private void OnTargetExit(GameObject target)
    {
    }

    private void OnTargetEnter(GameObject target)
    {
    }
}