﻿public class PlayerInfo
{
    public static PlayerInfo Instance
    {
        get { return instance == null ? instance = new PlayerInfo() : instance; }
    }

    private static PlayerInfo instance;
    
    public int wavePeriod = 4;
    public int waveRadius = 2;
}