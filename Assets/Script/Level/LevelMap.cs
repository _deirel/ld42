﻿using System;
using System.Collections.Generic;
using Objects;
using UnityEngine;

namespace Level
{
    public class LevelMap : MonoBehaviour
    {
        private Dictionary<String, List<GameObject>> map = new Dictionary<string, List<GameObject>>();
        
        private void Start()
        {
            var objects = GetComponentsInChildren<LevelObject>();
            foreach (var levelObject in objects)
            {
                var code = GetXZCode(levelObject.X, levelObject.Z);
                var list = map.ContainsKey(code) ? map[code] : map[code] = new List<GameObject>();
                list.Add(levelObject.gameObject);

                levelObject.onDestroy += OnLevelObjectDestroyCallback(levelObject);
            }
        }

        private Action OnLevelObjectDestroyCallback(LevelObject levelObject)
        {
            return () =>
            {
                var code = GetXZCode(levelObject.X, levelObject.Z);
                if (!map.ContainsKey(code))
                    return;

                map[code].Remove(levelObject.gameObject);
            };
        }

        public List<GameObject> GetObjectsOnXZ(int x, int z)
        {
            var code = GetXZCode(x, z);
            List<GameObject> list = null;
            map.TryGetValue(code, out list);
            return list;
        }

        private String GetXZCode(int x, int z)
        {
            return x + ":" + z;
        }
    }
}