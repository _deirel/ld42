﻿using System;
using JetBrains.Annotations;
using Level;
using Objects;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class Game
{
    public static Game Instance
    {
        get { return instance == null ? instance = new Game() : instance; }
    }

    private static Game instance;
    
    private String levelName;
    private LevelMap levelMap;
    private GameObject player;

    private Game()
    {
        NotificationCenter.AddHandler(GameNotifications.Restart, OnRestartLevel);
        NotificationCenter.AddHandler<string>(GameNotifications.StartLevel, OnStartLevel);
        NotificationCenter.AddHandler(GameNotifications.LevelPassed, OnLevelPassed);

        CheckLevelIsLoaded();
    }

    private void OnRestartLevel()
    {
        RestartLevel();
    }

    private void OnStartLevel(string levelName)
    {
        StartNewLevel(levelName);
    }

    private void OnLevelPassed()
    {
        Levels.SetPassed(levelName);
        StartNewLevel(Levels.StartLevelName);
    }

    private void CheckLevelIsLoaded()
    {
        if (SceneManager.GetActiveScene().name.IndexOf("Level", StringComparison.Ordinal) > -1)
        {
            OnLevelSceneLoaded(null);
        }
    }

    public void RestartLevel()
    {
        if (levelName == null)
            return;
        
        StartNewLevel(levelName);
    }

    public void StartNewLevel(String name)
    {
        SceneManager.LoadSceneAsync(name).completed += OnLevelSceneLoaded;
    }

    private void OnLevelSceneLoaded([CanBeNull] AsyncOperation op)
    {
        if (op != null)
        {
            op.completed -= OnLevelSceneLoaded;
        }

        levelName = SceneManager.GetActiveScene().name;

        var levelGameObject = GameObject.Find("Level");
        levelMap = levelGameObject.GetComponent<LevelMap>();
        player = levelGameObject.transform.Find("Player").gameObject;
    }
    
    public static LevelMap LevelMap
    {
        get { return Instance.levelMap; }
    }

    public static GameObject Player
    {
        get { return Instance.player; }
    }
}