﻿using UnityEngine;
using Utils;

namespace Objects
{
    public class StepBehaviorWaveSpawn : MonoBehaviour, IStepBehavior
    {
        public int period = 4;
        public int waveRadius = 2;
        
        private int stepCounter = 0;

        private void Start()
        {
            period = PlayerInfo.Instance.wavePeriod;
            waveRadius = PlayerInfo.Instance.waveRadius;
        }

        public void OnStep(int x, int z)
        {
            if (++stepCounter == period)
            {
                stepCounter = 0;
                SpawnWave(x, z);
            }
        }

        private void SpawnWave(int x, int z)
        {
            var waveGameObject = new GameObject();
            waveGameObject.name = "wave";
            waveGameObject.transform.parent = transform.parent;
            LevelUtils.SetObjectXZ(waveGameObject, x, z);

            waveGameObject.AddComponent<LevelObject>();

            var wave = waveGameObject.AddComponent<DamageWave>();
            wave.radius = waveRadius;
        }
    }
}