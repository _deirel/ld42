﻿using UnityEngine;

namespace Objects
{
    public class Health : MonoBehaviour
    {
       
        public bool isUndestructible = false;

        [SerializeField]
        private int maxHealth = 100;
        private int currentHealth;

        private void Start()
        {
            currentHealth = maxHealth;
        }

        public void ApplyDamage(int value)
        {
            if (isUndestructible)
                return;

            var effect = GetComponent<IHealthEffect>();
            currentHealth -= value;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                if (effect != null)
                {
                    OnDestruction();
                }
            }
            else if (effect != null)
            {
                effect.OnDamage();
            }
        }

        private void OnDestruction()
        {
            Destroy(gameObject);
        }

        public int CurrentHealth
        {
            get { return currentHealth; }
        }
        
        public int MaxHealth
        {
            get { return maxHealth; }
        }
    }
}