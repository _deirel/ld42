﻿namespace Objects
{
    public interface IHealthEffect
    {
        void OnDamage();
    }
}