﻿using UnityEngine;

namespace Objects
{
    public class DestructiveInteractionDamage : MonoBehaviour, IDestructiveInteraction
    {
        public void Interact(GameObject interactor)
        {
            var damager = interactor.GetComponent<IDamager>();
            var health = GetComponent<Health>();
            health.ApplyDamage(damager.Damage);
        }
    }
}