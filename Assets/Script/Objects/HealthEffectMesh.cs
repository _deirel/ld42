﻿using System.Collections.Generic;
using UnityEngine;

namespace Objects
{
    public class HealthEffectMesh : MonoBehaviour, IHealthEffect
    {
        public List<Mesh> meshes;
        
        public void OnDamage()
        {
            var health = GetComponent<Health>();
            var ratio = 1f - (float) health.CurrentHealth / health.MaxHealth;
            var index = Mathf.RoundToInt(ratio * meshes.Count);
            if (index > meshes.Count - 1)
                index = meshes.Count - 1;

            var meshFilter = GetComponentInChildren<MeshFilter>();
            meshFilter.mesh = meshes[index];
        }
    }
}