﻿using UnityEngine;

namespace Objects
{
    public interface IInteraction
    {
        void Interact(GameObject interactor);
        
        string InteractionObjectName { get; }
        
        string InteractionObjectUsage { get; }
    }
}