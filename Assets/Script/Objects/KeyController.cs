﻿using UnityEngine;

namespace Objects
{
    public class KeyController : MonoBehaviour
    {
        public ItemType itemType;

        private void Start()
        {
            var inventory = GetComponent<Inventory>();
            inventory.AddItems(itemType);
            switch (itemType)
            {
                case ItemType.KeyRed:
                    SetMaterialColor(Color.red);
                    break;
            
                case ItemType.KeyGreen:
                    SetMaterialColor(Color.green);
                    break;
            }
        }

        private void SetMaterialColor(Color color)
        {
            GetComponentInChildren<MeshRenderer>().material.color = color;
        }
    }
}