﻿using System;
using UnityEngine;
using Utils;

namespace Objects
{
    public class HealthEffectColor : MonoBehaviour, IHealthEffect
    {
        public Color color;
        
        private Material material;
        
        private void Start()
        {
            material = GetComponentInChildren<MeshRenderer>().material;
        }

        public void OnDamage()
        {
            var health = GetComponent<Health>();
            var ratio = (float) health.CurrentHealth / health.MaxHealth;
            var resultColor = Color.Lerp(color, material.color, ratio);
            material.color = resultColor;
        }
    }
}