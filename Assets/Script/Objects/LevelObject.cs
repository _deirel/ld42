﻿using System;
using UnityEngine;
using Utils;

namespace Objects
{
    public class LevelObject : MonoBehaviour
    {
        public event Action onDestroy;
        
        private int x;
        private int z;

        private void Awake()
        {
            x = LevelUtils.GetObjectX(gameObject);
            z = LevelUtils.GetObjectZ(gameObject);
        }

        private void OnDestroy()
        {
            if (onDestroy != null) onDestroy();
            onDestroy = null;
        }

        public int X
        {
            get { return x;  }
        }

        public int Z
        {
            get { return z; }
        }
    }
}