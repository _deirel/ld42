﻿using UnityEngine;
using Utils;

namespace Objects
{
    public class StepBehaviorPlayerMark : MonoBehaviour, IStepBehavior
    {
        private GameObject mark;
        private StepController _stepController;

        private void Start()
        {
            mark = GameObject.Find("PlayerMark");
            _stepController = GetComponent<StepController>();
            OnStep(_stepController.X, _stepController.Z);
        }

        public void OnStep(int x, int z)
        {
            LevelUtils.SetObjectXZ(mark, x, z);
        }
    }
}