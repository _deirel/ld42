﻿namespace Objects
{
    public interface IStepBehavior
    {
        void OnStep(int x, int z);
    }
}