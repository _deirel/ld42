﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Objects
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField]
        private List<ItemType> items = new List<ItemType>();

        public void AddItems(params ItemType[] items)
        {
            this.items.AddRange(items);
        }

        public bool HasItem(ItemType item)
        {
            return items.Contains(item);
        }

        public bool HasItems(params ItemType[] items)
        {
            var copy = new List<ItemType>(this.items);
            return items.All(item => copy.Remove(item));
        }

        public bool RemoveItems(params ItemType[] items)
        {
            if (!HasItems(items))
                return false;

            foreach (var item in items)
            {
                this.items.Remove(item);
            }
            
            return true;
        }

        public List<ItemType> RemoveAllItems()
        {
            var copy = new List<ItemType>(items);
            items.Clear();
            return copy;
        }
    }
}