﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Objects
{
    public class InteractionDoor : MonoBehaviour, IInteraction
    {
        public Mesh openedMesh;
        public List<ItemType> requiredItems;

        public void Interact(GameObject interactor)
        {
            var inventory = interactor.GetComponent<Inventory>();
            if (inventory.RemoveItems(requiredItems.ToArray()))
            {
                Destroy(GetComponentInChildren<Collider>());
                GetComponentInChildren<MeshFilter>().mesh = openedMesh;
            }
        }

        public string InteractionObjectName
        {
            get { return "Door"; }
        }

        public string InteractionObjectUsage
        {
            get { return "Unlock"; }
        }
    }
}