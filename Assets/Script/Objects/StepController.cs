﻿using UnityEngine;
using Utils;

namespace Objects
{
    public class StepController : MonoBehaviour
    {
        private int lastX;
        private int lastZ;
    
        private void Start()
        {
            lastX = LevelUtils.GetObjectX(gameObject);
            lastZ = LevelUtils.GetObjectZ(gameObject);
        }

        private void Update()
        {
            var newX = LevelUtils.GetObjectX(gameObject);
            var newZ = LevelUtils.GetObjectZ(gameObject);
            if (newX == lastX && newZ == lastZ)
                return;

            lastX = newX;
            lastZ = newZ;

            var behaviors = GetComponents<IStepBehavior>();
            foreach (var behavior in behaviors)
            {
                behavior.OnStep(lastX, lastZ);
            }
        }

        public int X
        {
            get { return lastX; }
        }

        public int Z
        {
            get { return lastZ; }
        }
    }
}