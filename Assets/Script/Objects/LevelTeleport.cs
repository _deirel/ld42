﻿using UnityEngine;

namespace Objects
{
    public class LevelTeleport : MonoBehaviour
    {
        public GameObject passedText;
        public string levelName;
        
        private void Start()
        {
            if (Levels.IsLevelPassed(levelName))
            {
                Destroy(GetComponentInChildren<Rotator>());
                passedText.SetActive(true);
            }
        }
    }
}