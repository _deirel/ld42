﻿using UnityEngine;

namespace Objects
{
    public class InteractionPick : MonoBehaviour, IInteraction
    {
        public string interactionName;
        public bool destroyOnPick = true;
        
        public void Interact(GameObject interactor)
        {
            var targetInventory = interactor.GetComponent<Inventory>();
            if (targetInventory == null)
                return;
            
            var inventory = GetComponent<Inventory>();
            var items = inventory.RemoveAllItems();
            targetInventory.AddItems(items.ToArray());

            if (destroyOnPick)
            {
                Destroy(gameObject);
            }
        }

        public string InteractionObjectName
        {
            get { return interactionName; }
        }

        public string InteractionObjectUsage
        {
            get { return "Pick Up"; }
        }
    }
}