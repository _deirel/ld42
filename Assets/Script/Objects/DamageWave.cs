﻿using System;
using System.Collections;
using UnityEngine;

namespace Objects
{
    public class DamageWave : MonoBehaviour, IDamager
    {
        public int radius;

        private int currentDistance;
        private int x0, z0;

        private void Start()
        {
            var levelObject = GetComponent<LevelObject>();
            x0 = levelObject.X;
            z0 = levelObject.Z;

            StartCoroutine(WavePropagation());
        }

        private IEnumerator WavePropagation()
        {
            var levelMap = Game.LevelMap;
            for (currentDistance = 0; currentDistance <= radius; currentDistance++)
            {
                ForPositionsAround(x0, z0, currentDistance, (x, z) =>
                {
                    var objects = levelMap.GetObjectsOnXZ(x, z);
                    if (objects == null)
                        return;
                    
                    foreach (var o in objects)
                    {
                        var interactions = o.GetComponents<IDestructiveInteraction>();
                        foreach (var interaction in interactions)
                        {
                            interaction.Interact(gameObject);
                        }
                    }
                });
                
                yield return new WaitForSeconds(0.1f);
            }
        }

        private static void ForPositionsAround(int x0, int z0, int distance, Action<int, int> callback)
        {
            if (distance == 0)
            {
                callback(x0, z0);
                return;
            }

            for (var x = -distance; x < distance; x++) callback(x0 + x, z0 - distance);
            for (var z = -distance; z < distance; z++) callback(x0 + distance, z0 + z);
            for (var x = distance; x > -distance; x--) callback(x0 + x, z0 + distance);
            for (var z = distance; z > -distance; z--) callback(x0 - distance, z0 + z);
        }

        public int Damage
        {
            get { return 100; }
        }
    }
    /*
     * *****
     * *   *
     * * X *
     * *   *
     * *****
     */
}