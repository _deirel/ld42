﻿using UnityEngine;

namespace Objects
{
    public class InteractionExit : MonoBehaviour, IInteraction
    {
        public void Interact(GameObject interactor)
        {
            NotificationCenter.PostNotification(GameNotifications.LevelPassed);
        }

        public string InteractionObjectName
        {
            get { return "Level Exit"; }
        }

        public string InteractionObjectUsage
        {
            get { return "Exit"; }
        }
    }
}