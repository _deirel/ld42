﻿using UnityEngine;

namespace Objects
{
    public interface IDestructiveInteraction
    {
        void Interact(GameObject interactor);
    }
}