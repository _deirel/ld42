﻿using UnityEngine;

namespace Objects
{
    public class InteractionLevelTeleport : MonoBehaviour, IInteraction
    {
        private string levelName;

        private void Start()
        {
            levelName = GetComponent<LevelTeleport>().levelName;
        }

        public void Interact(GameObject interactor)
        {
            NotificationCenter.PostNotification(GameNotifications.StartLevel, levelName);
        }

        public string InteractionObjectName
        {
            get { return "Level Teleport"; }
        }

        public string InteractionObjectUsage
        {
            get { return "Teleport To " + levelName; }
        }
    }
}