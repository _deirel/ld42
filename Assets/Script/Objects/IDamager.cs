﻿namespace Objects
{
    public interface IDamager
    {
        int Damage { get; }
    }
}