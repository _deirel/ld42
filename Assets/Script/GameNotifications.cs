﻿using System;

public class GameNotifications
{
    private const string Prefix = "Game";

    public static readonly string Restart = Prefix + "Restart";
    public static readonly string LevelPassed = Prefix + "LevelPassed";
    public static readonly string StartLevel = Prefix + "StartLevel";
}