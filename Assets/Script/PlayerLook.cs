﻿using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public bool needMouseDown = false;
    public float sensitivity = 1f;
    
    private static float MultiplierY = 2.5f;
    private static float MultiplierX = 2.5f;
    private Camera _camera;

    void Start()
    {
        _camera = GetComponentInChildren<Camera>();
    }
    
    void Update()
    {
        if (!needMouseDown || Input.GetKey(KeyCode.Mouse0))
        {
            var mouseX = Input.GetAxis("Mouse X") * MultiplierX * sensitivity;
            var mouseY = -Input.GetAxis("Mouse Y") * MultiplierY * sensitivity;
            transform.Rotate(Vector3.up, mouseX);     
            _camera.transform.Rotate(Vector3.right, mouseY);
        }
    }
}