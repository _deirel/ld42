﻿using System;

public class GUINotifications
{
    private const string Prefix = "GUI";

    public static readonly string SelectedObjectChange = Prefix + "SelectedObjectChange";
}