﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rate;

    void Update()
    {
        transform.Rotate(Vector3.up, Time.deltaTime * rate);
    }
}