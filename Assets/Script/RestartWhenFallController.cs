﻿using UnityEngine;

public class RestartWhenFallController : MonoBehaviour
{
    private GameObject player;

    private void Start()
    {
        player = Game.Player;
    }

    private void FixedUpdate()
    {
        if (player == null)
            return;

        if (player.transform.position.y < -10f)
        {
            player = null;
            NotificationCenter.PostNotification(GameNotifications.Restart);
        }
    }
}